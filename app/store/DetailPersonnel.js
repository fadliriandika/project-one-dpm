Ext.define('Pertemuan.store.DetailPersonnel', {
    extend: 'Ext.data.Store',
    storeId: 'detailpersonnel',
    alias: 'store.detailpersonnel',
    //autoLoad: true,
    //autoSync: true,

    fields: [
        'user_id', 'name', 'email', 'phone'
        //'foto', 'npm', 'name', 'email', 'phone', 'jenkel'
    ],

    /*data: { items: [
        { foto: 'fadli.jpg', jenkel: 'Laki-Laki' ,npm: "183510213", name: 'M Fadli Riandika', email: "fadlirindika@student.uir.ac.id", phone: "0822-8696-0852" },
        { foto: 'riri.jpg', jenkel: 'Perempuan' ,npm: "183510214", name: 'Riri Fitri Yanti', email: "riri@enterprise.com",  phone: "555-222-2222" },
        { foto: 'ricky.jpg', jenkel: 'Laki-Laki' ,npm: "183510215", name: 'Ricky', email: "ricky@enterprise.com", phone: "555-333-3333" },
        { foto: 'ziqri.jpg', jenkel: 'Laki-Laki' ,npm: "183510216", name: 'Ziqri', email: "ziqri@enterprise.com", phone: "555-444-4444" },
        { foto: 'fauzi.jpg', jenkel: 'Laki-Laki' ,npm: "183510217", name: 'Fauzi', email: "fauzi@enterprise.com", phone: "555-555-5555" }
    ]},*/

    proxy: {
        //type: 'memory',
        type: 'jsonp',
        api: {
            read: "http://localhost/MyApp_php/readDetailPersonnel.php"
        },
        
        reader: {
            type: 'json',
            rootProperty: 'items'
        }
    },

});
