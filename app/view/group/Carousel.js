Ext.define('Pertemuan.view.group.Carousel', {
    extend: 'Ext.Container',
    xtype: 'mycarousel',
    requires: [
        'Ext.carousel.Carousel'
    ],

    
    shadow: true,
    layout: {
        type: 'hbox',
        align: 'stretch'
    },
    /*defaults: {
        cls: 'demo-solid-background',
        flex: 1
    },*/
    items: [{
        xtype: 'carousel',
        width: 200,
        direction: 'vertical',  
        items: [{
            html: '<p>Swipe left to show the next card…</p>'
        },
        {
            html: '<p>You can also tap on either side of the indicators.</p>'
        },
        {
            html: 'Card #3'
        }]
    },  {
        xtype: 'fieldset',
        flex: 1,
        defaults: {
            labelAlign: 'top'
        },
        items: [
            {
                xtype: 'textfield',
                label: 'Title',
                labelAlign: 'placeholder'
            },
            {
                xtype: 'textfield',
                label: 'Price',
                labelAlign: 'placeholder'
            },
            {
                xtype: 'textfield',
                label: 'Specific Location (optional)',
                labelAlign: 'placeholder'
            },
            {
                xtype: 'textareafield',
                label: 'Description',
                labelAlign: 'placeholder'
            }
        ]
    }]
});