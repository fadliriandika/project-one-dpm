/**
 * This view is an example list of people.
 */
Ext.define('Pertemuan.view.main.List', {
    extend: 'Ext.grid.Grid',
    xtype: 'mainlist',

    title: 'Daftar Anggota Sencha',

    requires: [
        'Pertemuan.store.Personnel',
        'Ext.grid.plugin.Editable',
    ],

    plugins: [{
        type: 'grideditable'
    }],

    //store: {
        //type: 'personnel'
    //},
    bind: {
        store: '{personnel}'
    },

    viewModel: {
        stores:{
            personnel:{
                type: 'personnel'
            }
        }
    },

    columns: [
        { text: 'NPM',  dataIndex: 'user_id', width: 250 },
        { text: 'Nama',  dataIndex: 'name', width: 250, editable: true},
        { text: 'Email', dataIndex: 'email', width: 250, editable: true},
        { text: 'Telepon', dataIndex: 'phone', width: 150, editable: true }
    ],

    listeners: {
        select: 'onItemSelected'
    }
});
