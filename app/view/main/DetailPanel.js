Ext.define('Pertemuan.view.main.DetailPanel', {
    extend: 'Ext.Container',
    xtype : 'mydetail',
    requires: [
        'Ext.dataview.plugin.ItemTip',
        'Ext.plugin.Responsive',
        'Pertemuan.store.DetailPersonnel'
        //'Pertemuan.store.Personnel',
        //'Ext.field.Search'
    ],

    /*viewModel: {
        stores : {
            personnel: {
                type : 'personnel'
            }
        }
    },*/

    layout: 'fit',
    cls: 'ks-basic demo-solid-background',
    shadow: true,
    /*viewModel: {
        stores : {
            detailpersonnel: {
                type : 'detailpersonnel'
            }
        }
    },*/
    viewModel: {
        stores : {
            personnel: {
                type : 'personnel'
            }
        }
    },
    items: [{
        /*docked: 'top',
        xtype: 'toolbar',
        items: [
            {
                xtype: 'searchfield',
                placeHolder: 'Search',
                name: 'searchfield',
                listeners: {
                    change: function (me, newValue, oldValue, eOpts){
                        //alert(newValue);
                        personnelstore = Ext.getStore('personnel');
                        personnelstore.filter('name', newValue);
                    }
                }
            }
        ]
    },{*/
        
        xtype: 'dataview',
        id: 'mydetailpanel',
        scrollable: 'y',
        cls: 'dataview-basic',
        itemTpl: '<table style="border-spacing:3px;border-collapse:separate">' +
        //'<tr><td rowspan= 5 ><img class="photo" src="resources/images/{foto}" width=100></td></tr>' +
        '<tr><td><font size = 4><b>{name}</b></font><br><font color = grey>{user_id}</font><br><font color = grey>{email}</font><br><font color = grey>{phone}</font><br><button type=button onclick="onUpdatePersonnel({user_id})">Edit</button><button type=button onclick="onDeletePersonnel({user_id})">Hapus</button></td></tr>' + 
        '<hr>',
        bind: {
            store: '{personnel}'
        },
        //store: 'personnel',
        /*bind: {
            store: '{detailpersonnel}'
        },*/
        /*plugins: {
            type: 'dataviewtip',
            align: 'l-r?',
            plugins: 'responsive',
            
            // On small form factor, display below.
            responsiveConfig: {
                "width < 600": {
                    align: 'tl-bl?'
                }
            },
            width: 600,
            minWidth: 300,
            //delegate: '.photo',
            allowOver: true,
            anchor: true,
            bind: '{record}',
            tpl: '<table style="border-spacing:3px;border-collapse:separate">' + 
                    '<tr><td>NPM           : </td><td>{user_id}</td></tr>' +
                    '<tr><td>Nama          :</td><td>{name}</td></tr>' +
                    '<tr><td>Email         :</td><td>{email}</td></tr>' +
                    '<tr><td>Phone         :</td><td>{phone}</td></tr>' 
                
        }*/
    }]
});