/**
 * This class is the main view for the application. It is specified in app.js as the
 * "mainView" property. That setting causes an instance of this class to be created and
 * added to the Viewport container.
 *
 * TODO - Replace the content of this view to suit the needs of your application.
 */
Ext.define('Pertemuan.view.main.Main', {
    extend: 'Ext.tab.Panel',
    xtype: 'app-main',
    id : 'app-main',
    requires: [
        'Ext.MessageBox',

        'Pertemuan.view.main.MainController',
        'Pertemuan.view.main.MainModel',
        'Pertemuan.view.main.List',
        'Pertemuan.view.form.User',
        'Pertemuan.view.setting.BasicDataView',
        'Pertemuan.view.group.Carousel',
        'Pertemuan.view.form.Login',
        'Pertemuan.view.chart.Column',
        'Pertemuan.view.chart.Bubble',
        'Pertemuan.view.chart.Pie',
        'Pertemuan.view.tree.TreeList',
        'Pertemuan.view.tree.TreePanel',
        'Pertemuan.view.main.DetailPanel',
        'Pertemuan.view.form.FormPanel',
        'Pertemuan.view.chart.Bar',
        'Pertemuan.view.chart.ListBar'
    ],

    controller: 'main',
    viewModel: 'main',

    defaults: {
        tab: {
            iconAlign: 'top'
        },
        styleHtmlContent: true
    },

    tabBarPosition: 'bottom',

    items: [
        {
            xtype: 'toolbar',
            docked: 'top',
            items: [
                {
                    xtype: 'button',
                    text: 'Read',
                    ui: 'action',
                    scope: this,
                    //listeners: {
                        //tap : 'onReadClicked'
                    //}
                }
            ]
        },
        {
            title: 'Home',
            iconCls: 'x-fa fa-home',
            layout: 'fit',
            items: [{
                xtype: 'panel',
                layout: 'hbox',
                items:[{
                    xtype: 'mainlist',
                    flex: 2
                },{
                    xtype: 'mydetail',
                    flex: 1
                },{
                    xtype: 'editform',
                    flex: 1
                }]
            }]
        },{
            title: 'Users',
            iconCls: 'x-fa fa-user',
            layout: 'fit',
            items: [{
                xtype: 'user'
            }]
        },{
            title: 'Groups',
            iconCls: 'x-fa fa-users',
            layout: 'fit',
            items: [{
                xtype: 'mycarousel'
            }]
        },{
            title: 'Settings',
            iconCls: 'x-fa fa-cog',
            layout: 'fit',
            items: [{
                xtype: 'basicdataview'
            }]
        },{
            title: 'Chart',
            iconCls: 'x-fa fa-cog',
            layout: 'fit',
            items: [{
                xtype: 'panel',
                layout: 'vbox',
                items:[{
                    xtype: 'bar',
                    flex: 1
                },{
                    xtype: 'listbar',
                    flex: 1
                }]
                //xtype: 'bar'
                //xtype: 'pie-chart'
            }]
        },{
            title: 'Tree',
            iconCls: 'x-fa fa-cog',
            layout: 'fit',
            items: [{
                xtype: 'tree-panel'
            }]
        }
    ]
});
