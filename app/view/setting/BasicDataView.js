Ext.define('Pertemuan.view.setting.BasicDataView', {
    extend: 'Ext.Container',
    xtype : 'basicdataview',
    requires: [
        'Ext.dataview.plugin.ItemTip',
        'Ext.plugin.Responsive',
        'Pertemuan.store.Personnel',
        'Ext.field.Search'
    ],

    /*viewModel: {
        stores : {
            personnel: {
                type : 'personnel'
            }
        }
    },*/

    layout: 'fit',
    cls: 'ks-basic demo-solid-background',
    shadow: true,
    items: [{
        docked: 'top',
        xtype: 'toolbar',
        items: [
            {
                xtype: 'searchfield',
                placeHolder: 'Search',
                name: 'searchfield',
                listeners: {
                    change: function (me, newValue, oldValue, eOpts){
                        //alert(newValue);
                        personnelstore = Ext.getStore('personnel');
                        personnelstore.filter('name', newValue);
                    }
                }
            }
        ]
    },{
        xtype: 'dataview',
        scrollable: 'y',
        cls: 'dataview-basic',
        itemTpl: '<table style="border-spacing:3px;border-collapse:separate">' +
        '<tr><td rowspan= 5 ><img class="photo" src="resources/images/{foto}" width=100></td></tr>' +
        '<tr><td><font size = 4><b>{name}</b></font><br><font color = grey>{npm}</font></td></tr>' + 
        '<hr>',
        bind: {
            store: '{personnel}'
        },
        plugins: {
            type: 'dataviewtip',
            align: 'l-r?',
            plugins: 'responsive',
            
            // On small form factor, display below.
            responsiveConfig: {
                "width < 600": {
                    align: 'tl-bl?'
                }
            },
            width: 600,
            minWidth: 300,
            delegate: '.photo',
            allowOver: true,
            anchor: true,
            bind: '{record}',
            tpl: '<table style="border-spacing:3px;border-collapse:separate">' + 
                    '<tr><td>NPM           : </td><td>{npm}</td></tr>' +
                    '<tr><td>Nama          :</td><td>{name}</td></tr>' +
                    '<tr><td>Jenis Kelamin :</td><td>{jenkel}</td></tr>' + 
                    '<tr><td>Email         :</td><td>{email}</td></tr>' +
                    '<tr><td>Phone         :</td><td>{phone}</td></tr>' 
                
        }
    }]
});