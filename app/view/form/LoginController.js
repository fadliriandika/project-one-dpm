Ext.define('Pertemuan.view.form.LoginController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.login',

    onLogin: function () {
        var form = this.getView();
        var username = form.getFields('username').getValue();
        var password = form.getFields('password').getValue();
        //cek username password sesuai dengan database
        if (username && password) {
            if(username=="admin" && password=="admin123"){
                localStorage.setItem('loggedin', true);
                form.hide();
                Ext.Msg.alert('<center>Login Berhasil</center>','<center>Selamat Datang Sahabat</center>');
            }
            else{
                Ext.Msg.alert('<center>Peringatan</center>','<center>Username dan Password Salah</center>');
            }
        }
        
    }

})