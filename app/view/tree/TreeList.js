Ext.define('Pertemuan.view.tree.TreeList', {
    extend: 'Ext.grid.Tree',
    xtype: 'tree-list',
    requires: [
        'Ext.grid.plugin.MultiSelection'
    ],
    
    cls: 'demo-solid-background',
    shadow: true,

    viewModel: {
        type: 'tree-list'
    },

    bind: '{navItems}',

    listeners: {
        itemtap: function ( me, index, target, record, e, eOpts ){
            detailtree = Ext.getStore('personnel');
            //detailtree = Ext.getCmp('detailtree');
            detailtree.filter('jenkel',record.data.text);
            //detailtree.setHtml("<center><b>Tampilan "+ record.data.text+"</b></center>" + record.data.html);
            //alert("Anda Memilih " + record.data.text);
        }
    }
});